# NOTAS PARA LOS USUARIOS

## COMPILACION DEL PROGRAMA
Se ejecuta la siguiente instrución:  
make jar
### USO DEL CONCESIONARIO DE COCHES
Permite ejecutar las siguientes instrucciones:  
1. Mostrar concesionario:  
java -jar concesionario.jar list  
2.Mostrar ayuda:  
java -jar concesionario.jar help  
3.Añadir coche:  
java -jar concesionario.jar add <marca> <modelo> <precio> <matricula>  
4.Borrar coche:  
java -jar concesionario.jar rm <marca> <modelo> <precio> <matricula>  
5.Modificar coche:  
Los cuatro primeros atributos son del coche que el usuario desea borrar y los cuatro últimos pertenecen al coche que quieres añadir.  
java -jar concesionario.jar mdf <marca> <modelo> <precio> <matricula> <marca>
<modelo> <precio> <matricula>  



#### GENERAR JAVADOC
make javadoc
 

![alt text](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS16jtQq27r2SSOdhCoM0-ZSKlcHPBfNWtrLg&usqp=CAU/to/img.png)
