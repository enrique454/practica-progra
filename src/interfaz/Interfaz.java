/*
 * Copyright [2020] [ENRIQUE COLLADO MUÑOZ]
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
/**
 * @author ENRIQUE COLLADO MUÑOZ
 * */
package interfaz;
 
import dominio.*;
import java.util.Scanner;
import java.io.FileNotFoundException;
import java.io.File;
import java.io.FileWriter;
 
public class Interfaz{
	private static String HELP_TEXT = "Dirijase al readme";
	private static String NOMBRE_FICHERO = "concesionario.txt";
	private static String NOMBRE_CSV = "concesionarioDeCoches.csv";
 
	public static void procesarPeticion(String sentencia){
		String[] args = sentencia.split(" ");
		Concesionario concesionario = inicializarConcesionario(NOMBRE_FICHERO);
	       if(args[0].equals("help")){
	       System.out.println(HELP_TEXT);
	       } else if (args[0].equals("list")){
		       if(concesionario.toString().equals("")){
			       System.out.println("No hay ningun coche en el concesionario");
		        } else {
			       System.out.println(concesionario);
		       }
	        } else if (args[0].equals("add")){
		      Coche coche  = new Coche(args[1], args[2], Integer.parseInt(args[3]), Integer.parseInt(args[4]));
		      concesionario.annadirCoche(coche);
		      inicializarFichero(concesionario);
	       }
	       
	 	else if (args[0].equals("rm")){
                	 Coche coche = new Coche(args[1], args[2], Integer.parseInt(args[3]), Integer.parseInt(args[4]));
		     	 concesionario.borrarCoche(coche);
		      	inicializarFichero(concesionario);
	       }
	       else if (args[0].equals("mdf")){
		       Coche coche = new Coche(args[1], args[2], Integer.parseInt(args[3]), Integer.parseInt(args[4]));
		       concesionario.borrarCocheNuevo(coche);
		       inicializarFichero(concesionario);
		       Coche cocheNuevo = new Coche(args[5], args[6], Integer.parseInt(args[7]), Integer.parseInt(args[8]));
		       concesionario.annadirCocheNuevo(cocheNuevo);
		       inicializarFichero(concesionario);
	       }
	       

		
	 else if (args[0].equals("csv")){
	             generarCSV(concesionario);
	       
	       }
	}

	private static void inicializarFichero(Concesionario concesionario){
		try{
			FileWriter fw = new FileWriter(NOMBRE_FICHERO);
			fw.write(concesionario.toString());
			fw.close();
		} catch (Exception e){
			System.out.println(e);
		}
	}
        private static void generarCSV(Concesionario concesionario){
		try{
			FileWriter fw = new FileWriter(NOMBRE_CSV);
			fw.write(concesionario.toCSV());
			fw.close();
		} catch (Exception e){
			System.out.println(e);
		}
	}
 

	private static Concesionario inicializarConcesionario(String nombreFichero){
		Concesionario concesionario = new Concesionario();
		try{
			File file = new File(nombreFichero);
			Scanner sc = new Scanner(file);
			while(sc.hasNext()){
				String marca = sc.next();
				String modelo = sc.next();
				int precio = Integer.parseInt(sc.next());
				int matricula = Integer.parseInt(sc.next());
				Coche coche = new Coche(marca, modelo, precio, matricula);
				concesionario.annadirCoche(coche);
			}
			sc.close();
		} catch (FileNotFoundException e){
			inicializarFichero(concesionario);
		} catch (Exception e){
			System.out.println(e);
		}
		return concesionario;
	}
	/** @return devuelve el concesionario en el que se encuentran los coches
	 */
}

