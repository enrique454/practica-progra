
 /*
         *Copyright [2020] [ENRIQUE COLLADO MUÑOZ]
          Licensed under the Apache License, Version 2.0 (the "License");
          you may not use this file except in compliance with the License.
          You may obtain a copy of the License at
          http://www.apache.org/licenses/LICENSE-2.0
          Unless required by applicable law or agreed to in writing, software
          distributed under the License is distributed on an "AS IS" BASIS,
          WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
          See the License for the specific language governing permissions and
          limitations under the License.
*/

/**
 * @author Enrique Collado Muñoz
 *
 */
package dominio;

public class Coche{
	private String marca;
	private String modelo;
	private int precio;
	private int matricula;
 
	public Coche(){
		marca = "";
		modelo = "";
		precio = Integer.parseInt("0");
		matricula = Integer.parseInt("0");
	}
 
 
	public Coche(String marca, String modelo, int precio, int matricula){
		this.marca = marca;
		this.modelo = modelo;
		this.precio = precio;
		this.matricula = matricula; 
	}
	/**
	 * @param devuelve la marca, el modelo y el precio del coche
	 */
	public String getMarca(){
		return marca;
	}
	/**
	 *@return devuelve la marca del coche
	 */
	public void setMarca(){
		this.marca = marca;
	}
	public String getModelo(){
		return modelo;
	}
	/**
	 * @return devuelve el modelo del coche
	 */
	public void setModelo(){
		this.modelo = modelo;
	}
	public int getPrecio(){
		return precio;
	}
	/**
	 *@return devuelve el precio del coche
	 */
	public void setPrecio(){
		this.precio = precio;
	}
	 public void setMatricula(){
                this.matricula = matricula;
        }
        public int getMatricula(){
                return matricula;
        }

	public String toCSV(){
		return marca + ", " + modelo +  ", " + precio +  " "  + matricula + "\n";
	}	

	public String toString(){
		return marca + " " + modelo + " " + precio + " " + matricula + "\n" ;
	}

	@Override	
	public boolean equals(Object object){
		Coche coche = (Coche) object;
		if(matricula == coche.getMatricula()){
			return true;
		} else {
			return false;
		}
	}

	/**
	 *@return devuelve la marca, el modelo y el precio del coche con un salto de línea 
 */
 
 
}
