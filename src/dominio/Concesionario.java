
/*
  * Copyright [2020] [ENRIQUE COLLADO MUÑOZ]
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

/**
 * 
 * @author Enrique Collado Muñoz
 */ 
package dominio; 
import java.util.ArrayList;
 
 
public class Concesionario{
	private  ArrayList<Coche> coleccionCoches = new ArrayList<>();
 
	public void annadirCoche(Coche coche){
			coleccionCoches.add(coche);
	}
	public void borrarCoche(Coche coche){
		coleccionCoches.remove(coche);
		
	}
	public void annadirCocheNuevo(Coche cocheNuevo){
                coleccionCoches.add(cocheNuevo);
	}
	public void borrarCocheNuevo(Coche cocheNuevo){
		coleccionCoches.remove(cocheNuevo);
	}



        
        public String toCSV(){
		StringBuilder datos = new StringBuilder();
		for (Coche coche : coleccionCoches){
			datos.append(coche.toCSV());
		}
		return datos.toString();
	}


	/**
	 * 
	 * @param Se annade un objeto de tipo coche de la clase coche al concesionario
	 */
	public String toString() {
		StringBuilder datos = new StringBuilder();
		for (Coche coche : coleccionCoches){
			datos.append(coche);
		}
		return datos.toString();
	}
}
	/**
	 * 
	 * @return de los datos del coche
	 *
	 */

 
 
 
 
 
 
